#!/usr/bin/env python3

FILE = "items.csv"

IDMAP = "idmap.json"
CHANGES = "changes.json"
IGNORE = "ignore.json"

WITEMS = "items-add.db"

import csv
import random
import string
import json

def auratoshort(aura: str):
	aura = aura.strip().lower()
	if aura == "abjuration":
		return "abj"
	elif aura == "conjuration":
		return "con"
	elif aura == "divination":
		return "div"
	elif aura == "enchantment":
		return "enc"
	elif aura == "evocation":
		return "evo"
	elif aura == "illusion":
		return "ill"
	elif aura == "necromancy":
		return "nec"
	elif aura == "transmutation":
		return "trs"
	elif aura == "universal":
		return "uni"
	return ""

def removecss(text):
	return text.replace('<link rel="stylesheet"href="PF.css">', "")

def genid(size=16, chars=string.ascii_lowercase + string.ascii_uppercase + string.digits):
	return ''.join(random.choice(chars) for _ in range(size))

_idmapfile = open(IDMAP, "r")
_iddict = json.load(_idmapfile)
_idmapfile.close()
def getid(oid):
	# read foundry ID from IDMAP or generate and save if none has been found
	if oid not in _iddict.keys():
		_iddict[oid] = genid()
	return _iddict[oid]

_changefile = open(CHANGES, "r")
_changedict = json.load(_changefile)
_changefile.close()
def getchanges(id):
	if id not in _changedict.keys():
		return []
	return _changedict[id]["changes"]

def getcontextnotes(id):
	if id not in _changedict.keys():
		return []
	return _changedict[id]["contextNotes"]

# Ignore items that pf1 already has
_ignorefile = open(IGNORE, "r")
_ignorelist = json.load(_ignorefile)
_ignorefile.close()


with open(FILE, newline='') as csvfile:
	witems = open(WITEMS, "w")

	r = csv.DictReader(csvfile, delimiter=',', quotechar='"')

	cnt = 0
	i = {
		"_id": "",
		"name":"",
		"permission": {
			"default":0, 
			"F33BRJQnkFgpwaT1":3
		},
		"type": "equipment",
		"data": {
			"description": {
				"value": "",
				"chat": "",
				"unidentified":""
			},
			"tags":[],
			"quantity":1,
			"weight":0,
			"price":0,
			"identified":True,
			"hp": {
				"max":10,
				"value":10
			},
			"hardness":0,
			"carried":True,
			"unidentified": {
				"price":0,
				"name":""
			},
			"identifiedName":"",
			"cl": 0,
			"aura": {
				"custom": False,
				"school": ""
			},
			"changes": [
			],
			"changeFlags": {
				"loseDexToAC":False,
				"noStr":False,
				"noDex":False,
				"oneInt":False,
				"oneWis":False,
				"oneCha":False,
				"noEncumbrance":False,
				"mediumArmorFullSpeed":False,
				"heavyArmorFullSpeed":False
			},
			"contextNotes":[],
			"links": {
				"children":[]
			},
			"equipped":True,
			"equipmentType":"misc",
			"equipmentSubtype":"wondrous",
			"armor": {
				"value":0,
				"dex":"",
				"acp":0,
				"enh":0
			},
			"spellFailure":0,
			"slot":"wrists",
			"masterwork":False,
			"size":"med",
			"broken":False,
			"activation": {
				"cost":None,
				"type":""
			},
			"measureTemplate": {
				"type":""
			},
			"damage": {
				"parts":[],
				"critParts":[],
				"nonCritParts":[]
			},
			"attack": {
				"parts":[]
			},
			"actionType":"",
			"soundEffect":"",
			"attackParts":[]
		},
		"flags":{},
		"img":"systems/pf1/icons/items/inventory/dice.jpg"
	}

	for item in r:
		if int(item["id"]) in _ignorelist:
			print(f"Ignoring {item['Name']}")
			continue

		i["_id"] = getid(item["id"])
		i["name"] = item["Name"]
		i["data"]["identifiedName"] = item["Name"]
		i["data"]["description"]["value"] = removecss(item["FullText"])
		i["data"]["price"] = int(item["PriceValue"])
		i["data"]["weight"] = item["WeightValue"]
		i["data"]["changes"] = getchanges(i["_id"])
		i["data"]["contextNotes"] = getcontextnotes(i["_id"])
		i["data"]["aura"]["school"] = ""

		if item["Group"] == "Wondrous Item":
			i["data"]["equipmentType"] = "misc"
			i["data"]["equipmentSubType"] = "wondrous"
			i["data"]["slot"] = item["Slot"]
			if isinstance(item["CL"], int):
				i["data"]["cl"] = item["CL"]
			else:
				i["data"]["cl"] = 0
			if ' ' in item["Aura"]:
				i["data"]["aura"]["school"] = auratoshort(item["Aura"].split(' ')[1])
			witems.write(json.dumps(i) + "\n")
			continue
		elif item["Group"] == "Weapon":
			i["data"]["equipmentType"] = "weapon"
			continue
		elif item["Group"] == "Armor":
			pass
		elif item["Group"] == "Ring":
			pass
		elif item["Group"] == "Rod":
			pass
		elif item["Group"] == "Ammunition":
			pass
		elif item["Group"] == "Artifact":
			pass
		elif item["Group"] == "Staff":
			pass
		elif item["Group"] == "Potion":
			pass
		elif item["Group"] == "Cursed":
			pass
		elif item["Group"] == "Wand":
			pass
		elif item["Group"] == "Magical Tattoo":
			pass
		elif item["Group"] == "Shadow Piercing":
			pass
		elif item["Group"] == "Legendary Weapon":
			# Only one?
			pass
		else:
			print(f"Unkown Group: {item['Group']}")

	witems.close()


_idmapfile = open(IDMAP, "w")
json.dump(_iddict, _idmapfile)
_idmapfile.close()
print("Done.")